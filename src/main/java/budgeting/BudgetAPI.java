/***
 * File: BudgetAPI.java
 * 
 * Description: This file holds the BudgetAPI class. This is a class of objects 
 * that retrieves budgeting information such as daily cost per person from
 * the BudgetYourTrip.com API
 * 
 * Author: Ryan Ragnell
 */
package budgeting;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

import org.apache.http.HttpRequest;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class BudgetAPI {
	
	//Url to access API with key for API provided by BudgetYourTrip.com
	private String baseUrl = "http://www.budgetyourtrip.com/api/v3";
	private String APIKey = "ryan_ragnell@baylor.edu";
	//Location city to look for
	private String location;
	//Geocode of location to get budget
	private String geocode;
	//Daily cost per person of BudgetAPI object made for specific location
	private Double cost;
	
	//Apache variables needed to call API
	private HttpClient client;
	private HttpGet request;
	private HttpResponse response;
	
	/**
	 * Default constructor
	 */
	public BudgetAPI(){}
	
	/***
	 * Constructor with location String (city name)
	 * @param location
	 */
	public BudgetAPI(String location){
		//Replace all spaces so proper API call can be made
		location = location.replaceAll(" ", "%20");
		this.location = location;
	}
	
	/**
	 * Function: getGeocode()
	 * This function gets the geocode related to a specific city.
	 * BudgetYourTrip uses a geocode database to store cost information so
	 * cities with similar names aren't confused.
	 * 
	 * This sets the geocode of the BudgetAPI object
	 * @throws IOException
	 * @throws JSONException
	 */
	private void getGeocode() throws IOException, JSONException{
		client = new DefaultHttpClient();
		request = new HttpGet(this.baseUrl + "/search/locationdata/" + this.location);
		request.addHeader("X-API-KEY", this.APIKey);
		
		response = client.execute(request);
		
		BufferedReader reader = new BufferedReader(
				new InputStreamReader(response.getEntity().getContent()));
		
		StringBuilder result = new StringBuilder();
		String line = "";
		while((line = reader.readLine()) != null){
			result.append(line);
		}
		try{
			JSONObject data = new JSONObject(result.toString());
			JSONArray locationData = data.getJSONArray("data");
			JSONObject locationObject = (JSONObject) locationData.get(0);
			this.geocode = locationObject.get("geonameid").toString();
			//System.out.println(this.geocode);
		}catch(NullPointerException e1){
			this.geocode = "-1";
		}catch(org.json.JSONException e1){
			this.geocode = "-1";
		}
	}
	
	/**
	 * Function: getGeocode()
	 * This function gets the cost related to a specific geocode(city).
	 * BudgetYourTrip uses a geocode to get specific cost info.
	 * @return Double, the daily cost per person of a location
	 * @throws IOException
	 * @throws JSONException
	 */
	public Double getCost() throws IOException, JSONException{
		this.getGeocode();
		Double cost = 0.0;
		if(this.geocode != "-1"){
			
			client = new DefaultHttpClient();
			//System.out.println(this.baseUrl + "/costs/location/" + this.geocode);
			request = new HttpGet(this.baseUrl + "/costs/location/" + this.geocode);
			request.addHeader("X-API-KEY", this.APIKey);
			response = client.execute(request);
			
			BufferedReader reader = new BufferedReader(
					new InputStreamReader(response.getEntity().getContent()));
			
			StringBuilder result = new StringBuilder();
			String line = "";
			while((line = reader.readLine()) != null){
				result.append(line);
			}
			
			
			JSONObject jsonResponse = new JSONObject(result.toString());
			JSONArray costData = jsonResponse.getJSONArray("data");
			JSONObject totalCostObject = (JSONObject) costData.get(costData.length()-1);
			cost = totalCostObject.getDouble("value_midrange");
			//System.out.println(cost.toString());
			
		}
		else{
			cost = 0.0;
		}
		
		return cost;
	}
	
	public static void main(String args[]) throws IOException, JSONException{
		BudgetAPI budget = new BudgetAPI("Miami");
		budget.getCost();
	}
}
