/**
 * This file was edited by Travis Forkner
 * Read the license for more information on the original creator.
 * This file is open source.
 * code I edited will/wrote will be specified in comments
 */

package yelp;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.scribe.builder.ServiceBuilder;
import org.scribe.model.OAuthRequest;
import org.scribe.model.Response;
import org.scribe.model.Token;
import org.scribe.model.Verb;
import org.scribe.oauth.OAuthService;

import com.beust.jcommander.JCommander;
import com.beust.jcommander.Parameter;

/**
 * Code sample for accessing the Yelp API V2.
 * 
 * This program demonstrates the capability of the Yelp API version 2.0 by using the Search API to
 * query for businesses by a search term and location, and the Business API to query additional
 * information about the top result from the search query.
 * 
 * <p>
 * See <a href="http://www.yelp.com/developers/documentation">Yelp Documentation</a> for more info.
 * 
 */
public class YelpAPI {

  private static String API_HOST = "api.yelp.com";
  private static String DEFAULT_TERM = "Ski Resorts";
  private static String DEFAULT_LOCATION = "Denver, CO";
  private static int SEARCH_LIMIT = 3;
  private static final String SEARCH_PATH = "/v2/search";
  private static final String BUSINESS_PATH = "/v2/business";
  private JSONArray getData;

  /*
   * Update OAuth credentials below from the Yelp Developers API site:
   * http://www.yelp.com/developers/getting_started/api_access
   */
  public static final String CONSUMER_KEY = "cNV6ZBZXZg90Agi30bhVrA";
  public static final String CONSUMER_SECRET = "BnBENlBWHXXJa3iGaiQRm11ATcs";
  public static final String TOKEN = "zzc9WR1BGlnkKFOBWPDnByB0eBMYeV6A";
  public static final String TOKEN_SECRET = "-BpFAXIVPvObLOR7nQxhzLiy5rc";

  OAuthService service;
  Token accessToken;

  /**
   * Made by Travis Forkner
   * 
   * This function is used to change what is being searched for.
   * From the defualt values...
   * 
   * @param term - change what is being searched for 
   * @param cityState - change city and state
   * @param searchAmount - change amount of items to be searched
   */
  
  public YelpAPI(){}

  /**
   * Made by Travis Forkner
   *
   * yelpApi constructor
   *
   * @param term
   * @param cityState
   * @param searchAmount
   */
  public YelpAPI(String term, String cityState, int searchAmount){
      DEFAULT_TERM = term;
      SEARCH_LIMIT = searchAmount;
      DEFAULT_LOCATION = cityState;
  }
  /**
   * Made by Travis Forkner
   * 
   * This function initializes the yelpAPI service and access token from scribe library
   * to allow the oath authentication to work.
   * 
   */
  private void initializeAPI(){
      this.service =
              new ServiceBuilder().provider(TwoStepOAuth.class).apiKey(CONSUMER_KEY)
                  .apiSecret(CONSUMER_SECRET).build();
          this.accessToken = new Token(TOKEN, TOKEN_SECRET);
  }
  /**
   * Made by Travis Forkner
   *
   * This function runs after yelp has been initialized return ing the rank of a given location.
   */
  public double getRating(){
    getData = query();
    JSONObject outputBusiness = new JSONObject();
    JSONObject tempBusiness = new JSONObject();
    outputBusiness = (JSONObject) getData.get(0);
    
    return (double) outputBusiness.get("rating");
  }
  /**
   * Made by Travis Forkner
   *
   * This function runs after yelp get rank has ran and will return a link to an image of the locations rank.
   */
  public String getRankImgLink(){
	    JSONObject outputBusiness = new JSONObject();
	    JSONObject tempBusiness = new JSONObject();
	    outputBusiness = (JSONObject) getData.get(0);

	    return (String) outputBusiness.get("rating_img_url");
	 }
  
  /**
   * Setup the Yelp API OAuth credentials.
   * 
   * @param consumerKey Consumer key
   * @param consumerSecret Consumer secret
   * @param token Token
   * @param tokenSecret Token secret
   */
  public YelpAPI(String consumerKey, String consumerSecret, String token, String tokenSecret) {
    this.service =
        new ServiceBuilder().provider(TwoStepOAuth.class).apiKey(consumerKey)
            .apiSecret(consumerSecret).build();
    this.accessToken = new Token(token, tokenSecret);
  }
  
  /**
   * Creates and sends a request to the Search API by term and location.
   * <p>
   * See <a href="http://www.yelp.com/developers/documentation/v2/search_api">Yelp Search API V2</a>
   * for more info.
   * 
   * @param term <tt>String</tt> of the search term to be queried
   * @param location <tt>String</tt> of the location
   * @return <tt>String</tt> JSON Response
   */
  public String searchForBusinessesByLocation(String term, String location) {
    OAuthRequest request = createOAuthRequest(SEARCH_PATH);
    request.addQuerystringParameter("term", term);
    request.addQuerystringParameter("location", location);
    request.addQuerystringParameter("limit", String.valueOf(SEARCH_LIMIT));
    return sendRequestAndGetResponse(request);
  }

  /**
   * Creates and sends a request to the Business API by business ID.
   * <p>
   * See <a href="http://www.yelp.com/developers/documentation/v2/business">Yelp Business API V2</a>
   * for more info.
   * 
   * @param businessID <tt>String</tt> business ID of the requested business
   * @return <tt>String</tt> JSON Response
   */
  public String searchByBusinessId(String businessID) {
    OAuthRequest request = createOAuthRequest(BUSINESS_PATH + "/" + businessID);
    return sendRequestAndGetResponse(request);
  }

  /**
   * Creates and returns an {@link OAuthRequest} based on the API endpoint specified.
   * 
   * @param path API endpoint to be queried
   * @return <tt>OAuthRequest</tt>
   */
  private OAuthRequest createOAuthRequest(String path) {
    OAuthRequest request = new OAuthRequest(Verb.GET, "http://" + API_HOST + path);
    return request;
  }

  /**
   * Sends an {@link OAuthRequest} and returns the {@link Response} body.
   * 
   * @param request {@link OAuthRequest} corresponding to the API request
   * @return <tt>String</tt> body of API response
   */
  private String sendRequestAndGetResponse(OAuthRequest request) {
    //System.out.println("Querying " + request.getCompleteUrl() + " ...");
    this.service.signRequest(this.accessToken, request);
    Response response = request.send();
    return response.getBody();
  }

  /**
   * Edited by Travis
   *
   * Queries the Search API based on the command line arguments and takes the first result to query
   * the Business API.
   * 
   * @param yelpApi <tt>YelpAPI</tt> service instance
   * @param yelpApiCli <tt>YelpAPICLI</tt> command line arguments
   */
  private static JSONArray queryAPI(YelpAPI yelpApi, YelpAPICLI yelpApiCli) {
    String searchResponseJSON =
        yelpApi.searchForBusinessesByLocation(yelpApiCli.term, yelpApiCli.location);
    
    JSONParser parser = new JSONParser();
    JSONObject response = null;
    try {
      response = (JSONObject) parser.parse(searchResponseJSON);
    } catch (ParseException pe) {
      System.out.println("Error: could not parse JSON response:");
      System.out.println(searchResponseJSON);
      System.exit(1);
    }
    JSONArray businesses = (JSONArray) response.get("businesses");
    return businesses;
  }

  /**
   * Command-line interface for the sample Yelp API runner.
   */
  public static class YelpAPICLI {
    @Parameter(names = {"-q", "--term"}, description = "Search Query Term")
    public String term = DEFAULT_TERM;

    @Parameter(names = {"-l", "--location"}, description = "Location to be Queried")
    public String location = DEFAULT_LOCATION;
  }

  /**
   * Edited by Travis
   *
   * Main entry for sample Yelp API requests.
   * <p>
   * After entering your OAuth credentials, execute <tt><b>run.sh</b></tt> to run this example.
   */
  public JSONArray query() {
    YelpAPICLI yelpApiCli = new YelpAPICLI();
    this.initializeAPI();
    return queryAPI(this, yelpApiCli);
  }
}
