/***
 * File: TrailAPI.java
 * 
 * Description: This file holds the TrailAPI class. This class is used to 
 * retrieve all ski resort locations in a given state as well as populate
 * the data of each location object and then populate a location catalog.
 * 
 * Because Trail is responsible for the resorts that we rank, it is given
 * the responsibility of fully populating those resorts and then ranking them.
 * 
 * Author: Ryan Ragnell
 */package trail;


import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Map;

import models.*;

import org.json.*;
import org.json.simple.parser.ParseException;

import airfare.AirportFinder;
import airfare.QPX;
import budgeting.BudgetAPI;

import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.JsonNode;
import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.exceptions.UnirestException;

import ranking.BudgetRank;
import ranking.DescriptionRank;
import yelp.YelpAPI;


public class TrailAPI {
	
	//API information -- the url and the key
	private final static String key = "o7NF5Wa8i4mshKkKpleekpS2JSSmp16zVAgjsnOGZGHNv1Cl7k";
	private static String baseUrl = "https://trailapi-trailapi.p.mashape.com/?limit=1000"
									+ "&q[activities_activity_type_name_eq]=snow+sports&q[name_cont]=resort";
	
	//State and country query to append to the url once userdata is received
	private static String countryQuery = "&q[country_cont]=";
	private static String stateQuery = "&q[state_cont]=";
	
	//Data of the response
	private JSONObject data;
	//Places within the data
	private JSONArray places;
	
	//Catalog of Locations unique to each call to Trail
	public LocationCatalog catalog;
	
	/**
	 * Default constructor
	 */
	public TrailAPI(){
		catalog = new LocationCatalog();
	}
	
	public void setCountry(String country){
		country = country.replaceAll(" ", "+");
		baseUrl += countryQuery + country;
	}
	
	public void setState(String state){
		state = state.replaceAll(" ", "+");
		baseUrl += stateQuery + state;
	}

	/**
	 * Function: getResorts()
	 * This function populates the JSONObject data and JSONArray places for the
	 * TrailAPI object
	 * @return String, the body of the response from the API request
	 * @throws IOException
	 * @throws JSONException
	 * @throws UnirestException
	 */
	private String getResorts() throws IOException, JSONException, 
													UnirestException {
		HttpResponse<String> response = Unirest.get(baseUrl)
				.header("X-Mashape-Key", key)
				.header("Accept", "text/plain")
				.asString();
		
		data = new JSONObject(response.getBody());
		places = data.getJSONArray("places");
		
		return response.getBody();
	}
	
	
	
	/****
	 * Function: parseLocations()
	 * 
	 * This function gets all of the resorts for the given UserData input.
	 * This is responsible for creating instances of all other API's so that
	 * data can be retrieved from them and used to populate locations and 
	 * calculate ranking.
	 * 
	 * @param input
	 * @param locations
	 * @return Boolean, true if origin airport exists, false otherwise
	 * @throws JSONException
	 * @throws IOException
	 * @throws ParseException
	 * @throws UnirestException
	 */
	public Boolean parseLocations(UserData input, LocationCatalog locations) 
			throws JSONException, IOException, ParseException, UnirestException{
		
		getResorts();
	
		//All other API's and classes to rank and get data
		DescriptionRank descRankAlg = new DescriptionRank();
		BudgetRank budgetRanker = new BudgetRank();
		QPX airfare = new QPX();
		BudgetAPI budget = new BudgetAPI();
		YelpAPI yelpData = new YelpAPI();
		
		//Set departure and return for airfare information
		Calendar cal = Calendar.getInstance();
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
		cal.setTime(input.getStartingDate());
		String startDate = format.format(cal.getTime());
		cal.add(Calendar.DATE, input.getLengthOfStay());
		String returnDate = format.format(cal.getTime());
		
		//QPX information to find airfare
		airfare.setDeparture(startDate);
		airfare.setReturn(returnDate);
		
		//If origin exists
		if(airfare.setOrigin(input.getAirportCode())){
			System.out.println("The airport code for origin is : " + input.getAirportCode());
			airfare.setPassengers(input.getNumberOfGuests());
			
			for(int i = 0; i < places.length(); i++){
				
				Location newLocation = new Location();
				JSONObject currentPlace = places.getJSONObject(i);
				
				/**
				 * Populate all data members of the location with data from the
				 * current place in the JSONArray places
				 */
				newLocation.setCity(currentPlace.getString("city"));
				newLocation.setState(currentPlace.getString("state"));
				newLocation.setCountry(currentPlace.getString("country"));
				newLocation.setName(currentPlace.getString("name"));
				newLocation.setLatitude(currentPlace.getDouble("lat"));
				newLocation.setLongitude(currentPlace.getDouble("lon"));
				newLocation.setTrailDescription(currentPlace.getJSONArray("activities").getJSONObject(0)
												.getString("description"));
				newLocation.setTrailRating(currentPlace.getJSONArray("activities").getJSONObject(0)
												.getDouble("rating"));
				
				
				//Find daily cost per person && airport info IF AND ONLY IF budget is cared about
				if(input.getMaxPrice() != 0){
					budget = new BudgetAPI(newLocation.getCity());
					newLocation.setDailyCostPerPerson(budget.getCost());
					
					//Find nearest airport, set code and name, add code to QPX list of airports
					AirportFinder findAirport = new AirportFinder(newLocation.getLatitude(), 
																	newLocation.getLongitude());
					
					newLocation.setAirportCode(findAirport.latLonAirportQuery().getCode());
					newLocation.setAirportName(findAirport.latLonAirportQuery().getName()); 
					airfare.addDestinationCode(newLocation.getAirportCode());
				}
				
				//Set the Yelp rating
				yelpData = new YelpAPI(newLocation.getName(), newLocation.getCity() + ", " + newLocation.getState() , 1);
				newLocation.setYelpRating(yelpData.getRating());
				newLocation.setYelpRatingLink(yelpData.getRankImgLink());
				
				//Add location to catalog
				locations.addLocation(newLocation);
			}
			
			//Only if they care about budget!!
			Map<String, Integer> pricing = null;
			if(input.getMaxPrice() != 0){
				//Run QPX to get airfare to each airport
				 pricing = airfare.getPricing();
			}
			
			//Get airfare pricing for each city, add to total cost
			
			/**
			 * Calculate ranking! Conditions for if budget is cared about
			 */
			for(int i = 0; i < locations.getSize(); i++){
				Location currentLocation = locations.get(i);
				
				//Only if they care about budget!!
				if(input.getMaxPrice() != 0){
					Integer price = pricing.get(currentLocation.getAirportCode());
					currentLocation.setAirfareCost(price);
					System.out.println("Airfare for " + input.getNumberOfGuests() + " people is " + currentLocation.getAirfareCost());
					
					Double dailyCost = currentLocation.getDailyCostPerPerson();
					System.out.println("Daily Cost before check for 0 is " + currentLocation.getCity() + " is " + currentLocation.getDailyCostPerPerson());
					if(dailyCost > 75){
						currentLocation.setHasDailyCost(true);
						currentLocation.setTotalTripCost(price + (dailyCost*input.getNumberOfGuests()*input.getLengthOfStay()));
					}
					else{
						
						/**
						 * This is the allowed daily cost per person given the flight info
						 * This occurs only when only airfare can be found and no daily cost info
						 * This is not used in the budget ranking, but is just displayed to the user
						 */
						currentLocation.setDailyCostPerPerson((input.getMaxPrice()-price)
																/input.getLengthOfStay()
																/input.getNumberOfGuests());
						//Only airfare included in "total trip cost"
						currentLocation.setTotalTripCost(price.doubleValue());
					}	
					
					currentLocation.setRanking(new Double(budgetRanker.calcRanking(currentLocation, input)));
				}
				
				//Calculate ranking from description
				currentLocation.setRanking(descRankAlg.calcRanking(currentLocation.getTrailDescription(),
										input.getResortSize(),input.getFamilyFriendly(),input.getDifficulty()));
				//Add 2 times Yelp rating to rank (increase gap between location ranks)
				currentLocation.setRanking(currentLocation.getYelpRating() * 2);
				
			}
		
			//Initial sort
			locations.sortLocations();
			
			//Tiebreaker with Yelp rating
			for(int i = 0; i < locations.getSize()-1; i++){
				if(Math.abs(locations.get(i).getRanking() -  locations.get(i+1).getRanking()) <= 0.01){
					
					if(locations.get(i).getYelpRating() > locations.get(i+1).getYelpRating()){
						locations.get(i).setRanking(0.01);
					}
					else{
						locations.get(i+1).setRanking(0.01);
					}
				}
			}
			
			
			//Final sort of all locations based on ranking!
			locations.sortLocations();
			return true;
		}
		else{
			return false;
		}
	}
}