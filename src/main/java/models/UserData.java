package models;


import java.util.Calendar;
import java.util.Date;

/**
 * Created by klamse on 10/20/15.
 */
public class UserData {
    private Double maxPrice;
    private String state;
    private Boolean familyFriendly;
    private String resortSize;
    private Date startingDate;
    private int lengthOfStay;
    private int numberOfGuests;
    private String difficulty;
    private String airportCode;
    private String cityState;

    public UserData(){
        maxPrice = 0.0;
        state = new String();
        familyFriendly = true;
        resortSize = new String();
        startingDate = new Date();
        lengthOfStay = 1;
        numberOfGuests = 1;
        difficulty = new String();
        airportCode = new String();
        cityState = new String();
    }

    public void setMaxPrice(Double maxPrice) {
        this.maxPrice = maxPrice;
    }

    public Double getMaxPrice() {
        return maxPrice;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getState(){
        return state;
    }

    public void setFamilyFriendly(Boolean familyFriendly) {
        this.familyFriendly = familyFriendly;
    }

    public Boolean getFamilyFriendly() {
        return familyFriendly;
    }

    public void setResortSize(String resortSize) {
        this.resortSize = resortSize;
    }

    public String getResortSize() {
        return resortSize;
    }

    public void setStartingDate(Date startingDate) {
        this.startingDate = startingDate;
    }

    public Date getStartingDate() {
        return startingDate;
    }

    public void setLengthOfStay(int lengthOfStay) {
        this.lengthOfStay = lengthOfStay;
    }

    public int getLengthOfStay() {
        return lengthOfStay;
    }

    public void setNumberOfGuests(int numberOfGuests) {
        this.numberOfGuests = numberOfGuests;
    }

    public int getNumberOfGuests() {
        return numberOfGuests;
    }

    public void setDifficulty(String difficulty) {
        this.difficulty = difficulty;
    }

    public String getDifficulty() {
        return difficulty;
    }

    public void setAirportCode(String airportCode) {
        this.airportCode = airportCode;
    }

    public String getAirportCode() {
        return airportCode;
    }

    public void setCityState(String cityState) {
        this.cityState = cityState;
    }

    public String getCityState() {
        return cityState;
    }

    @Override
    public String toString() {
        return  "Max Price: " + maxPrice + "\n"
                + "State: " + state + "\n"
                + "Family Friendly: " + familyFriendly + "\n"
                + "Resort Size: " + resortSize + "\n"
                + "Starting Date: " + startingDate + "\n"
                + "Length of Stay: " + lengthOfStay + "\n"
                + "Number of Guests: " + numberOfGuests + "\n"
                + "Difficulty: " + difficulty + "\n"
                + "Aiport Code: " + airportCode + "\n"
                + "City, State: " + cityState + "\n";
    }
}
