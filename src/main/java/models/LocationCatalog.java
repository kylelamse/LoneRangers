/***
 * File: LocationCatalog.java
 * 
 * Description: This file holds the LocationCatalog class. This is a class
 * that holds an ArrayList of Location objects so that the MainController
 * can simply run through all of the locations sequentially.
 * 
 * Author: Ryan Ragnell
 */
package models;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

public class LocationCatalog {
	//ArrayList holding all locations
	private ArrayList<Location> locations;
	
	public LocationCatalog(){
		locations = new ArrayList<Location>();
	}
	
	public ArrayList<Location> getLocations(){
		return locations;
	}
	
	public void addLocation(Location newLocation){
		locations.add(newLocation);
	}
	
	public void removeLocation(Location location){
		locations.remove(location);
	}
	
	public int getSize(){
		return locations.size();
	}
	
	public Location get(int index){
		return locations.get(index);
	}
	
	/**
	 * Function: empty()
	 * This function is used to empty the ArrayList when a user wants to search
	 * for another set of locations.
	 */
	public void empty(){
		locations.removeAll(locations);
	}
	
	/***
	 * Function: sortLocations()
	 * This function sorts all locations in the locations ArrayList by their
	 * ranking score.
	 */
	public void sortLocations(){
		for(int i = 0; i < locations.size(); i++){
				Collections.sort(locations, new Comparator<Location>() {
					@Override 
						public int compare(Location l1, Location l2) {
			            return l2.getRanking().compareTo(l1.getRanking());
			        }
				});
		}
	}
}
