/***
 * File: Location.java
 * 
 * Description: This file holds the Location class, a class used to create
 * locations out of information obtained from various API's, with most info
 * coming from TrailAPI.
 * 
 * Author: Ryan Ragnell
 */
package models;

import java.util.Comparator;


public class Location {
	//City, state, country of the location to create
	private String city;
	private String state;
	private String country;
	private String name;
	
	//Airport information so QPX can be called to get airfare information
	private String airportName;
	private String airportCode;
	private Integer airfareCost;
	
	//Budgeting information for budget ranking
	private Double dailyCostPerPerson;
	private Double totalTripCost;
	private Double budgetCostDiff;
	
	//Description of the location/resort given by Trail
	private String trailDescription;
	

	private Double latitude;
	private Double longitude;
	
	//A Boolean to help the budget ranker to know to account for daily cost
	private Boolean hasDailyCost;
	
	private Double trailRating;
	private Double yelpRating;
	private String yelpRatingLink;
	
	//Ranking score incremented by rankers
	private Double ranking;
	
	
	/**
	 * Default constructor
	 * 
	 * Some values set to 0 initially to avoid NullPointerExceptions
	 */
	public Location(){
		ranking = 0.0;
		hasDailyCost = false;
		dailyCostPerPerson = 0.0;
		totalTripCost = 0.0;
		budgetCostDiff = 0.0;
		airfareCost = 0;
		
	}
	
	/*****
	 * 
	 * All getter and setter functions created by Eclipse code completion.
	 * 
	 */
	
	public  String getCity() {
		return this.city;
	}
	
	public void setCity(String city) {
		this.city = city;
	}
	
	public String getState() {
		return this.state;
	}
	
	public void setState(String state) {
		this.state = state;
	}
	
	public String getCountry() {
		return this.country;
	}

	public void setCountry(String country) {
		this.country = country;
	}
	
	public String getName() {
		return this.name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public String getAirportName() {
		return this.airportName;
	}
	
	public void setAirportName(String airport) {
		this.airportName = airport;
	}
	
	public String getAirportCode() {
		return airportCode;
	}

	public void setAirportCode(String airportCode) {
		this.airportCode = airportCode;
	}

	public Integer getAirfareCost() {
		return airfareCost;
	}

	public void setAirfareCost(Integer price) {
		this.airfareCost += price;
	}

	public Double getDailyCostPerPerson() {
		return dailyCostPerPerson;
	}

	public void setDailyCostPerPerson(Double dailyCostPerPerson) {
		this.dailyCostPerPerson += dailyCostPerPerson;
	}

	public Double getTotalTripCost() {
		return totalTripCost;
	}

	public void setTotalTripCost(Double totalTripCost) {
		this.totalTripCost += totalTripCost;
	}

	public Double getBudgetCostDiff() {
		return budgetCostDiff;
	}

	public void setBudgetCostDiff(Double budgetCostDiff) {
		this.budgetCostDiff = budgetCostDiff;
	}

	public String getTrailDescription() {
		return this.trailDescription;
	}
	
	public void setTrailDescription(String trailDescription) {
		this.trailDescription = trailDescription;
	}
	
	public Double getLatitude() {
		return this.latitude;
	}
	
	public void setLatitude(Double latitude) {
		this.latitude = latitude;
	}
	
	public Double getLongitude() {
		return this.longitude;
	}
	
	public void setLongitude(Double longitude) {
		this.longitude = longitude;
	}
	
	public Boolean getHasDailyCost() {
		return hasDailyCost;
	}

	public void setHasDailyCost(Boolean hasDailyCost) {
		this.hasDailyCost = hasDailyCost;
	}

	public Double getTrailRating() {
		return this.trailRating;
	}
	
	public void setTrailRating(Double trailRating) {
		this.trailRating = trailRating;
	}
	
	public Double getYelpRating() {
		return this.yelpRating;
	}
	
	public void setYelpRating(Double yelpRating) {
		this.yelpRating = yelpRating;
	}
	public String getYelpRatingLink() {
		return yelpRatingLink;
	}

	public void setYelpRatingLink(String yelpRatingLink) {
		this.yelpRatingLink = yelpRatingLink;
	}

	public Double getRanking() {
		return this.ranking;
	}
	
	public void setRanking(Double ranking) {
		this.ranking = this.ranking + ranking;
	}
	
	public String toString(){

		String data = "City: " + 
						this.city + "\nState: " +
						this.state + "\nCountry: " +
						this.country + "\nName: " +
						this.name + "\nTrail Description: " +
						this.trailDescription + "\nLat: " +
						this.latitude.toString() + "\nLon: " +
						this.longitude.toString() + "\nTrail Rating: " +
						this.trailRating.toString() + "\nAirport Name: " +
						this.getAirportName() + "\nAirport Code: " + 
						this.getAirportCode() + "\nRANKING  -----> " +
						this.getRanking().toString() + "\n\n";
		
		return data;
	}

	
}
