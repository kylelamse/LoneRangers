 package main;

/**
 * @author The Lone Rangers
 * Ski Vacation Finer
 * Software Engineering 2, CSI 3372
 * Ryan Ragnell, Kyle Lamse, Austin MacDonald, Travis Forkner, Taylor Bradshaw
 */
public class Main {
	public static void main(String[] args) {
		System.setProperty("environment", "dev");
		Application.run(args);
	}
}
