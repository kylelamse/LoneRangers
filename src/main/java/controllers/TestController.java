package controllers;

import java.io.IOException;

import models.LocationCatalog;
import models.UserData;

import org.json.JSONException;
import org.json.simple.parser.ParseException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.stereotype.Service;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import trail.TrailAPI;
import yelp.YelpAPI;
import airfare.QPX;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.mashape.unirest.http.exceptions.UnirestException;

@Controller
public class TestController {
	@Autowired
	private FooService fooService;
	
	LocationCatalog catalog = new LocationCatalog();

	@RequestMapping("/test")
	public String testLanding(@RequestParam(value = "param1", required = false) String value1, @RequestParam(value = "param2") String value2, Model model) {
		model.addAttribute("data1", "We have some data passed to our view!");

		String result = fooService.foo(value1, value2);
		model.addAttribute("serviceResult", result);
		return "test";
	}

	@Service 
	public static class FooService {
		public String foo(String value1, String value2) {
			String result = value1;
			return result;
		}
	}
	
	@RequestMapping("/QPXTest")
	public String QPXTest(Model model) throws IOException, ParseException {		
		QPX flights = new QPX();
		ObjectMapper mapper = new ObjectMapper();
		Object jsonString = mapper.readValue(flights.QPXOutput(), Object.class);
		
		model.addAttribute("QPXStuff", mapper.writerWithDefaultPrettyPrinter().writeValueAsString(jsonString));
		
		return "QPX";
	}
	
	@RequestMapping("/yelpTest")
    public String yelpTest(Model model) throws IOException, JSONException {
	    YelpAPI getData = new YelpAPI("family friendly ski", "Denver, CO", 10);
	      

	    model.addAttribute("yelpstuff", getData.query());
	    
        return "yelp";
    }

	@RequestMapping("/trailTest")
	public String trailTest(Model model) throws IOException, JSONException, UnirestException, ParseException {
		TrailAPI trail = new TrailAPI();
		trail.setCountry("United States");
		trail.setState("Colorado");
		StringBuilder output = new StringBuilder();
		//trail.getResorts();
		UserData input = new UserData();
		input.setMaxPrice(1000.0);
		input.setResortSize("small");
		input.setDifficulty("intermediate");
//		input.setState("CO");
		trail.parseLocations(input, catalog);
		for(int i=0; i < catalog.getSize(); i++){
			output.append("Location " + i );
			System.out.println("Location " + i + "\n");
			output.append(catalog.get(i).toString());
			System.out.println(catalog.get(i).toString() + "\n");
		}
		model.addAttribute("trailStuff", output.toString());
		return "trail";
	}
	
}