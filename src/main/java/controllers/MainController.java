/*********************************************************
 * Filename: MainController.java
 * This is the main controller for the application. It sends the user to 
 * 	the appropriate webpage (html file) depending on what is requested
 */
package controllers;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;

import models.Location;

import models.LocationCatalog;
import models.UserData;

import org.json.JSONException;
import org.json.simple.parser.ParseException;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import trail.TrailAPI;
import yelp.YelpAPI;
import airfare.QPX;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.mashape.unirest.http.exceptions.UnirestException;


@Controller
public class MainController {


	/***
	 * This is the initial get function to get the userdata
	 * @param model, the model to be added to
	 * @return String, the html file
	 */
	@RequestMapping(value="/", method=RequestMethod.GET)
	public String skifinderForm(Model model) {
		model.addAttribute("userdata", new UserData());
		return "index";
	}


	/*****
	 * This is the post function to map to the appropriate response html
	 * 
	 * @param userdata, the input from the user
	 * @param model, the model to be added to
	 * @return String, the html file to go to
	 * @throws IOException
	 * @throws JSONException
	 * @throws UnirestException
	 * @throws ParseException
	 */
	@RequestMapping(value="/", method=RequestMethod.POST)
	public String skifinderSubmit(@ModelAttribute UserData userdata, Model model)
			throws IOException, JSONException, UnirestException, ParseException {

		StringBuilder output = new StringBuilder();
		UserData input = new UserData();
		
		/***
		 * Populate the UserData object with all criteria entered by user
		 * Airport code will be checked when parsing
		 */
		input.setMaxPrice(userdata.getMaxPrice());
		input.setState(userdata.getState());
		input.setFamilyFriendly(userdata.getFamilyFriendly());
		input.setResortSize(userdata.getResortSize());
		input.setStartingDate(userdata.getStartingDate());
		input.setLengthOfStay(userdata.getLengthOfStay());
		input.setNumberOfGuests(userdata.getNumberOfGuests());
		input.setDifficulty(userdata.getDifficulty());
		input.setAirportCode(userdata.getAirportCode());
		
		/**
		 * Create TrailAPI object. Trail will create other API's
		 */
		TrailAPI trail = new TrailAPI();
		trail.setCountry("United States");
		trail.setState(userdata.getState());
		
		//Locations are created and inserted in the catalog related to the Trail object
		if(trail.parseLocations(input, trail.catalog) && userdata.getAirportCode() != ""){
		
			model.addAttribute("locations", trail.catalog.getLocations());
			
			//If no locations are found, return an error and back to start screen
			if(trail.catalog.getSize() == 0){
				model.addAttribute("userdata", new UserData());
				return "index-location-error";
			}
	
			//Testing output
			for(int i=0; i < trail.catalog.getSize(); i++){
				output.append("Location " + i );
				System.out.println("Location " + i + "\n");
				output.append(trail.catalog.get(i).getCity());
				System.out.println(trail.catalog.get(i).getCity() + "\n");
			}
			return "new-results";
		}
		else{
			/**
			 * Else, the origin airport given did not exist
			 */
			model.addAttribute("userdata", new UserData());
			return "index-origin-error";
		}
		
	}
	
}

