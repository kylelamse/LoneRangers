package ranking;

import org.junit.Test;

import ranking.DescriptionRank;

/**
 * Created by trfor_000 on 11/20/2015.
 */
public class DescriptionRankTest {
    @Test
    public void TestNull(){
        System.out.println("DescriptionRankTest::TestNull");
        DescriptionRank des = new DescriptionRank();
        //program does not fail on null values
        des.calcRanking(null, null, null, null);
    }

    @Test
    public void TestImproperValues(){
        System.out.println("DescriptionRankTest::TestImproperValues");
        DescriptionRank des = new DescriptionRank();
        //program does not fail on bad inputs

        //proper inputs should be
        //0 - a string containing location description
        //1 - an string from - small medium large
        //2 - an boolean from - true false
        //3 - an string from - easy intermediate hard
        des.calcRanking("", "", false, "");
        des.calcRanking("", "wrong", false, "bad");
    }
    @Test
    public void TestImproperStringFormatTrailCount(){
        System.out.println("DescriptionRankTest::TestImproperStringFormatTrailCount");
        DescriptionRank des = new DescriptionRank();
        //program does not fail on bad inputs

        //if string does not contain trail count information but uses proper phrasing
        // the program should not fail
        des.calcRanking(" trails 1.00", "small", true, "hard");
        des.calcRanking(" trails    ", "small", true, "hard");
        des.calcRanking(" trails sdafg", "small", true, "hard");
        des.calcRanking(" lifts sdadsad", "small", true, "hard");
        des.calcRanking(" runs sadsad", "small", true, "hard");
    }

    @Test
    public void TestImproperStringFormatOther(){
        System.out.println("DescriptionRankTest::TestImproperStringFormatOther");
        DescriptionRank des = new DescriptionRank();
        //program does not fail on bad inputs

        //if string does not contain trail count information but uses proper phrasing
        // the program should not fail
        des.calcRanking(" % beginner 1.00 % beginner", "small", true, "hard");
        des.calcRanking(" % easiest    ", "small", true, "hard");
        des.calcRanking(" % intermediate sdafg", "small", true, "hard");
        des.calcRanking(" hard % hard sda% harddsad", "small", true, "hard");
        des.calcRanking(" runs sadsrunsad", "small", true, "hard");
    }


}
