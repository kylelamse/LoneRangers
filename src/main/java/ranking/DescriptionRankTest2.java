package ranking;

import org.junit.Test;

import ranking.DescriptionRank;
import yelp.*;
import static org.junit.Assert.assertEquals;

/**
 * Created by trfor_000 on 11/20/2015.
 */
public class DescriptionRankTest2 {
    @Test
    public void TestGettingProperCountMultiVariable(){
        System.out.println("DescriptionRankTest2::TestGettingProperCountMultiVariable");
        DescriptionRank des = new DescriptionRank();
        //program will take in description assert if trail count or lif count is wrong.
        //also trail %

        System.out.println("---Testing Generic Input");
        des.calcRanking("Angel Fire Resort takes skiers and snowboarders down 2,077 feet from a summit elevation of 10,677 feet. <br />The area receives 210 inches of snowfall annually. Snowmaking supplements the annual snowfall on 52% of the mountain and 85% of the beginner terrain.<br /><br />Angel Fire has 11.5 km (over 7 miles) of cross-country terrain, split into beginner, intermediate, and advanced sections. The resort also boasts 74 trails for freestyle skiing and snowboarding, 26% of which are beginner, 50% intermediate, and 24% advanced. 5 lifts and 2 Wondercarpets access the trails. Popular trails include Nitro, Baa-da-boom, and Fly Home.", "small", true, "hard");
        assertEquals(new Double(26), des.getBegCount());
        assertEquals(new Double(50), des.getIntCount());
        assertEquals(new Double(24), des.getAdvCoun());
        assertEquals(new Double(0), des.getHardestCount());
        assertEquals(new Double(5), des.getLiftCount());
        assertEquals(new Double(74), des.getTrailCount());

        System.out.println("---Testing empty Input");
        des.calcRanking( "" , "small", true, "hard");
        assertEquals(new Double(0), des.getBegCount());
        assertEquals(new Double(0), des.getIntCount());
        assertEquals(new Double(0), des.getAdvCoun());
        assertEquals(new Double(0), des.getHardestCount());
        assertEquals(new Double(0), des.getLiftCount());
        assertEquals(new Double(0), des.getTrailCount());

        System.out.println("---Testing Generic Input");
        des.calcRanking( "This massive ski resort has plenty for skiers and snowboarders in grand proportions. Northstar at Tahoe rises to a summit of 8,610 feet and sits at a base elevation of 6,330 feet. <br /><br />The ski resort has 3,000 skiable acres of terrain covered in 350 inches of snowfall annually. 1,500 acres are supplemented by artificial snowmaking. <br /><br />Northstar boasts 93 trails accessed by 19 lifts. The lifts service the mountain at a total uphill capacity of 34,799 skiers and snowboarders per hour. The terrain is rated 13% beginner, 60% intermediate, and 27% advanced. Trails include Lumberjack, Logger's Loop, and Burnout." , "small", true, "hard");
        assertEquals(new Double(13), des.getBegCount());
        assertEquals(new Double(60), des.getIntCount());
        assertEquals(new Double(27), des.getAdvCoun());
        assertEquals(new Double(0), des.getHardestCount());
        assertEquals(new Double(19), des.getLiftCount());
        assertEquals(new Double(93), des.getTrailCount());

        System.out.println("---Testing Generic Input");
        des.calcRanking( "With a lofty summit elevation of 9,800 feet and an average annual snowfall of over 500 inches, Kirkwood has plenty of snow and high altitude thrills for everyone. <br /><br />Skiers and snowboarders experience 2,000 in vertical drop on trails that span over 2,300 acres in skiable terrain. 14 lifts provide access to the 72 trails at an uphill capacity of 19,905 people per hour. <br /><br />The 72 trails are rated 15% beginner, 50% intermediate, 20% advanced, and 15% expert. The longest trail is 2.5 miles. Other trails include Headwaters, Buckboard, and Devil's Draw." , "small", true, "hard");
        assertEquals(new Double(15), des.getBegCount());
        assertEquals(new Double(50), des.getIntCount());
        assertEquals(new Double(35), des.getAdvCoun());
        assertEquals(new Double(0), des.getHardestCount());
        assertEquals(new Double(14), des.getLiftCount());
        assertEquals(new Double(72), des.getTrailCount());


    }

    @Test
    public void TestRepetedSingleVariable(){
        System.out.println("DescriptionRankTest2::TestRepetedSingleVariable");
        DescriptionRank des = new DescriptionRank();

        System.out.println("---Testing repeated key word: TRAIL");
        des.calcRanking( "trails trails trails 5.0 trails" , "small", true, "hard");
        assertEquals(new Double(5), des.getTrailCount());

        System.out.println("---Testing repeated key word: RUNS");
        des.calcRanking( "runs runs sadasd runs 5.0 runs" , "small", true, "hard");
        assertEquals(new Double(5), des.getTrailCount());

        System.out.println("---Testing repeated key word: BEGINNER");
        des.calcRanking( "beginner % beginner sadasd% beginner 5.0 % beginner 6.0% beginner" , "small", true, "hard");
        assertEquals(new Double(6), des.getBegCount());

    }
}
