package ranking;


import javafx.util.Pair;
import java.lang.Object;
import javax.persistence.criteria.CriteriaBuilder;
import java.util.ArrayList;

/**
 * Created by trforkner_000 on 11/1/2015.
 */
public class DescriptionRank {
    String Description;
    static ArrayList<Pair<String, Pair<Integer, Integer> > > sizeCon;
    static ArrayList<Pair<String, Pair<Integer, Integer> > > familyCon;
    static ArrayList<Pair<String, Pair<Integer, Integer> > > diffCon;

    //lift information and trail percentage
    static final String LIFTS = " lifts";
    static final String TRAILS = " trails";
    static final String RUNS = " runs";
    static final String BEGINNER = "% beginner";
    static final String BEGTEST = "% of which are beginner";
    static final String EASIEST = "% easiest";
    static final String INTERMEDIATE = "% intermediate";
    static final String INTERMEDIATE2 = "% is intermediate";
    static final String MOREDIFFICULT = "% more difficult";
    static final String ADVANCED = "% advanced";
    static final String MOSTDIFFICULT = "% most difficult";
    static final String EXPERT = "% expert";
    static final String EXPERT2 = "% is expert";
    Double liftCount, trailCount, begCount, intCount, advCount, hardestCount;

    static final Integer SMALL = 0;
    static final Integer MEDIUM = 1;
    static final Integer LARGE = 2;

    static final Integer YES = 1;
    static final Integer NO = 0;

    static final Integer EASY = 0;
    static final Integer INTERMIDIATE = 1;
    static final Integer HARD = 2;

    int sizeAc;
    int familyAc;
    int diffAc;

    /**
     * Initialize dictionary and ranking stats
     */
    public DescriptionRank(){
        sizeAc = familyAc = diffAc = 0;
        liftCount = trailCount = begCount = intCount = advCount = hardestCount = 0.0;
        Description = "Winter Park Resort is an expansive ski area that offers 3,060 skiable acres of terrain over 5 designated ski areas. The terrain receives 348 inches of snowfall annually.  The various ski areas have different elevations and vertical drops. Winter Park has a summit elevation of 10,700 feet and a vertical drop of 1,700 feet. Mary Jane has a summit elevation of 12,060 feet and a vertical drop of 2,610 feet. Vasquez Ridge sits at 10,700 feet and has a vertical drop of 1,214 feet. Parsenn Bowl sits at 12,060 feet and has a vertical drop of 1,712 feet. And Vasquez Cirque has a summit of 11,900 feet and a vertical drop of 1,500 feet.  Winter Park Resort boasts 143 trails rated 8% beginner, 17% intermediate, 19% advanced, 53% most difficult, and 3% expert only. 25 lifts service the trails at an uphill capacity of 36,920 people per hour. Snowmaking supplements 26 of the trails. Trails include White Rabbit, Black Coal, and Stage Coach";
        //initialize dictionaries
        sizeCon = new ArrayList<Pair<String, Pair<Integer, Integer> > >();
        familyCon = new ArrayList<Pair<String, Pair<Integer, Integer> > >();
        diffCon = new ArrayList<Pair<String, Pair<Integer, Integer> > >();
        // initialize dictionary entries
        // key word/key association/value ranking

        //===size dictionary===
        //--LARGE words
        sizeCon.add(new Pair("large",new Pair( LARGE, 8)));
        sizeCon.add(new Pair("huge",new Pair( LARGE, 8)));
        sizeCon.add(new Pair("big",new Pair( LARGE, 8)));
        sizeCon.add(new Pair("boasts", new Pair( LARGE, 8)));
        //--SMALL words
        sizeCon.add(new Pair("small",new Pair( SMALL, 8)));
        sizeCon.add(new Pair("little",new Pair( SMALL, 8)));
        sizeCon.add(new Pair("tiny",new Pair( SMALL, 8)));

        //===familly dictionary===
        //--YES words
        familyCon.add(new Pair("family",new Pair(YES, 4)));
        familyCon.add(new Pair("all ages",new Pair(YES, 4)));
        familyCon.add(new Pair("lessons",new Pair(YES, 4)));
        familyCon.add(new Pair("all abilities",new Pair(YES, 4)));


        //===difficulty dictionary===
        //--HARD words
        diffCon.add(new Pair("expert",new Pair(HARD, 8)));
        //--INTERMIDIATE words
        diffCon.add(new Pair("all levels",new Pair(INTERMIDIATE, 8)));
        //--EASY words
        diffCon.add(new Pair("easy",new Pair(EASY, 8)));
    }

    public Double getLiftCount(){
        return liftCount;
    }
    public Double getTrailCount(){
        return trailCount;
    }
    public Double getBegCount(){
        return begCount;
    }
    public Double getIntCount(){
        return intCount;
    }
    public Double getAdvCoun(){
        return advCount;
    }
    public Double getHardestCount(){
        return hardestCount;
    }

    /**
     *  This function calculate rank based on user criteria if the criteria is blank it ill use default values to prevent
     *  crashing.
     *
     * @param in - the description to be parsed
     * @param size - the size of the resort
     * @param family - if the resort should be family friendly
     * @param difficulty - the percentiage of runs difficulty
     * @return - the rank calculated from these inputs
     */
    public Double calcRanking(String in, String size, Boolean family, String difficulty) {
        if (in != null) {
        	
        	Integer sizeChoice;
        	Integer familyChoice;
        	Integer diffChoice;
        	
        	if(size.equals("small")){
        		sizeChoice = 0;
        	}
        	else if(size.equals("medium")){
        		sizeChoice = 1;
        	}
        	else{
        		sizeChoice = 2;
        	}
        	
        	if(family){
        		familyChoice = 1;
        	}
        	else{
        		familyChoice = 0;
        	}
        	
        	if(difficulty.equals("easy")){
        		diffChoice = 0;
        	}
        	else if(difficulty.equals("intermediate")){
        		diffChoice = 1;
        	}
        	else{
        		diffChoice = 2;
        	}
        	
        	
            //===DICTIONARY_RANKING===
            Description = in;
            int wordNotFoundCounter = 0;
            //--FAMILY
            for (int i = 0; i < familyCon.size(); i++) {
                String test = familyCon.get(i).getKey();
                if (in.contains(test)) {
                    if (familyChoice == YES) {
                        familyAc += familyCon.get(i).getValue().getValue();
                        i = familyCon.size();
                    }
                    else if(familyChoice == NO){
                        i = familyCon.size();
                    }
                }
                else{
                    wordNotFoundCounter++;
                }
            }

            if(wordNotFoundCounter == familyCon.size() && familyChoice == NO){
                familyAc += 4;
            }

            Double rank = 0.0;
            generalCount();
            //===PARSED_DATA_RANKING===
            //--SIZE
            if(LARGE.equals(sizeChoice)) {
                if(trailCount > 120){
                    rank += 8;
                }
            }
            else if(MEDIUM.equals(sizeChoice)) {
                if(trailCount <= 120 && trailCount > 80){
                    rank += 8;
                }

            }
            else if(SMALL.equals(sizeChoice)) {
                if (trailCount <= 80) {
                    rank += 8;
                }
            }
            //--DIFFICULTY
            if(HARD.equals(diffChoice)){
                if(advCount >= begCount + intCount){
                    rank += 6*(1+((advCount)/100.0));
                }
                else{
                    rank += 6*((advCount)/100.0);
                }

      
            }
            else if(INTERMIDIATE.equals(diffChoice)){
                if(intCount >= advCount + begCount) {
                    rank += 6 * (1+((intCount) / 100.0));
                }
                else{
                    rank += 6 * ((intCount) / 100.0);
                }
            }
            else if(EASY.equals(diffChoice)){
                if(begCount >= advCount + intCount) {
                    rank += 6 * (1+((begCount) / 100.0));
                }
                else{
                    rank += 6 * ((begCount) / 100.0);
                }
            }
            return rank + familyAc;
        }
        return -1.0;
    }
    /**
     * Helper
     * this function is called several times by general count
     * to find the # associated with specific words.
     */
    private Double findDouble(String find, Double startingPoint){
        int end = Description.indexOf(find);
        Double out = new Double(0);
        while (end != -1 && out.equals(0.0)) {
            if (end != -1) {
                String subStr = Description.substring(0, end);
                int begin = subStr.lastIndexOf(' ');
                if (begin != -1) {
                    try {
                        out = Double.valueOf(subStr.substring(begin + 1, end));
                    } catch (NumberFormatException e) {
                        end = Description.indexOf(find, end + 1);
                    }
                }
                else{
                    end = Description.indexOf(find, end + 1);
                }
            }
        }
        return out + startingPoint;
    }
    /**
     * This function gets the values of trail count lift count and percentages of trail type
     */
    private void generalCount() {
    	sizeAc = familyAc = diffAc = 0;
        liftCount = trailCount = begCount = intCount = advCount = hardestCount = 0.0;
        if (Description != null) {
            liftCount = findDouble(LIFTS, liftCount);
            trailCount = findDouble(TRAILS, trailCount);
            if(trailCount.equals(0.0)){
                trailCount = findDouble(RUNS, trailCount);
            }
            begCount = findDouble(BEGINNER, begCount);
            begCount = findDouble(BEGTEST, begCount);
            begCount = findDouble(EASIEST, begCount);
            
            intCount = findDouble(INTERMEDIATE, intCount);
            intCount = findDouble(MOREDIFFICULT, intCount);
            intCount = findDouble(INTERMEDIATE2, intCount);
            
            advCount = findDouble(ADVANCED, advCount);
            advCount = findDouble(MOREDIFFICULT, advCount);
            advCount = findDouble(EXPERT, advCount);
            advCount = findDouble(EXPERT2, advCount);
            
            if(begCount != new Double(-1) && intCount != new Double(-1) && advCount != new Double(-1) && begCount + intCount + advCount < 100){
                hardestCount = 100 - (begCount + intCount + advCount);
            }
            
            if(begCount == 0.0 && intCount > 0 && advCount > 0){
            	begCount = 100 - intCount - advCount;
            }
            else if(intCount == 0.0 && begCount > 0 && advCount > 0){
            	intCount = 100 - begCount - advCount;
            }
            else if(advCount == 0.0 && begCount > 0 && intCount > 0){
            	advCount = 100 - begCount - intCount;
            }
        }
    }
}
