/***
 * File: BudgetRank.java
 * 
 * Description: This file holds the BudgetRank class. This class is used to 
 * to calculate ranking score associated with budget. This is only if the user
 * doesn't enter 0 for their budget, indicating they do not care about budget.
 * 
 * Author: Ryan Ragnell
 */
package ranking;

import models.Location;
import models.UserData;

public class BudgetRank {
	public BudgetRank(){}
	
	/***
	 * Function: calcRanking
	 * This function takes in the location in question and the input to 
	 * calculate the appropriate score to add to the ranking for the location.
	 * @param loc
	 * @param input
	 * @return Double, the addition in ranking
	 */
	public Double calcRanking(Location loc, UserData input){
		Double rankingScore = 0.0;
		Double totalCost = loc.getTotalTripCost();
		Double budget = input.getMaxPrice();
		Double diff = loc.getBudgetCostDiff();
		Double diffPercentage = 0.0;
		
		if(loc.getHasDailyCost()){
			diffPercentage = totalCost/budget;
			
			diffPercentage = 1 - diffPercentage;
			if(diffPercentage >= 1){
				diffPercentage = 1.0;
			}
			else if(diffPercentage <= -1){
				diffPercentage = -1.0;
			}
			rankingScore += 10 * diffPercentage;
		}
		else{
			budget *= .25;
			diffPercentage = totalCost/budget;
			
			
			diffPercentage = 1 - diffPercentage;
			if(diffPercentage >= 1){
				diffPercentage = 1.0;
			}
			else if(diffPercentage <= -1){
				diffPercentage = -1.0;
			}
			rankingScore += 10 * diffPercentage;	
		}
		
		System.out.println("City: " + loc.getCity() + "  Total Cost: " + totalCost + "  Budget: " + budget + "  Ranking: " + rankingScore);
		return rankingScore;
	}
	
}
