package airfare;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Iterator;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

/**
 * @author Taylor
 *
 */

class Flight {
   private String carrier;
   private String flight_number;
   public Flight( ) {}
   public Flight( JSONObject obj ) {
      //      System.out.println( "Flight Keyset" + obj.keySet( ) );
      carrier = obj.get( "carrier" ).toString( );
      flight_number = obj.get( "number" ).toString( );
   }
   public String getCarrier( ) {return carrier;}
   public String getFlightNumber( ) {return flight_number;}
   public String toString( ) {
      return "Flight # " + getFlightNumber( ) + " Carrier: " + getCarrier( );
   }
   public void print( ) {
      System.out.println( toString() );
   }
}
class Time {
   private LocalDateTime time;
   private static DateTimeFormatter dateFormatter = DateTimeFormatter.ofPattern( "yyyy-MM-dd" );
   private static DateTimeFormatter formatter = DateTimeFormatter.ofPattern( "yyyy-MM-dd HH:mm" );
   private static DateTimeFormatter display_formatter = DateTimeFormatter.ofPattern( "MM-dd-yyyy HH:mm" );

   public Time ( String date_time ) {
      String date = date_time.substring( 0, 10 ) + " " + date_time.substring( 11, 16 );
      time = LocalDateTime.parse( date, formatter );
   }
   public String toString( ) {
      return "" + time.format( display_formatter );
   } 
   public String getDate() {
      return time.format(dateFormatter) + "";
   }
}

public class Itinerary {
   private float total_price; 
   private Integer passengerCount;
   
   public Itinerary( ) {}
   public Itinerary( JSONObject obj ) { 

      // Figure out the pricing for this itinerary
      String tmpStr = obj.get( "saleTotal" ).toString( );
      total_price = Float.parseFloat( tmpStr.substring( 3 ) );
      //      System.out.println( "Total Price: $" + total_price );

      // How many passengers are counted?
      JSONArray arr = (JSONArray) obj.get("pricing");
      JSONObject objj = ( JSONObject ) ((JSONObject) arr.get(0)).get( "passengers" );
      passengerCount = Integer.parseInt( objj.get( "adultCount" ).toString( ) );
   }

   public float getTotal_price() {
      return total_price;
   }

   public void print( ) { 
      System.out.println("Itinerary Cost: $" + this.getTotal_price() + " for " + passengerCount + " participant(s)");
   }
}
