package airfare;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.TreeSet;
import java.lang.Object;

import org.json.JSONException;
import org.json.simple.*;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import airfare.*;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.ObjectMapper;
/**
 * @author Taylor
*/
class TripSet {

   private Iterator it;
   private ArrayList<Itinerary> itineraries;
   private boolean flightsAvailable;
   private void setFlightsAvailable(boolean available) {
      flightsAvailable = available;
   }
   public boolean areFlightsAvailable() { return this.flightsAvailable; }

   public TripSet() {
      setFlightsAvailable(false);
   }
   /**
    * Given trips section of a QPX return object, create itineraries from the results
   */
   public TripSet(JSONObject obj) {
      parse_trips(obj);
   }
   /**
    * Creates itineraries if there is relevant flight data
   */
   public void parse_trips(JSONObject obj) { 
      // Compile global data for query
	   System.out.println("Parse_Trips, obj.get(data):" + obj.get("data"));
      JSONObject globalData = (JSONObject)obj.get("data");
      checkForTrips(globalData);
      
      if (!this.areFlightsAvailable()) {
         itineraries = null;
      }
      else {
      // Get itinerary solutions
         JSONArray itineraries_raw = (JSONArray) obj.get("tripOption"); 
         constructItineraries(itineraries_raw);
      }
   }
   /**
    * Given trip options, pulls Cities involved in the flights. 
    * If there are any cities, flightsAvailable set to true, otherwise set to false
    */
   public void checkForTrips(JSONObject trip_data) {
      JSONArray trip_data_city = (JSONArray) trip_data.get("city");
      if (trip_data_city == null) {
         // No itineraries available
         this.setFlightsAvailable(false);
         return;
      }
      this.setFlightsAvailable(true); 
   }

   /**
    * Accessor for the itineraries produced
   */
   public ArrayList<Itinerary> getItineraries() {
      return this.itineraries;
   }
   /**
    * How many itineraries exist?
   */
   public int getItineraryCount() { 
      if (itineraries != null) return itineraries.size();
      else return 0;
   }
   
   /**
    * Construct itineraries from the JSONArray 
   */
   public void constructItineraries(JSONArray arr){
      this.itineraries = new ArrayList<Itinerary>(); 
      for (int i = 0; i < arr.size(); i++) {
         itineraries.add(new Itinerary((JSONObject) arr.get(i)));
      }
   }
   
   /**
    * Returns the cost of the flight. If a flight does not exist, -1 is returned
   */
   public Integer getLowestPrice() {
      if (!this.areFlightsAvailable()) {
         return -1;
      }
      else {
         // Prints Cost and # of participants
//         it = itineraries.iterator();
//         Itinerary tmp;
//         while (it.hasNext()) {
//            tmp = ( Itinerary ) it.next( );
//            tmp.print( );
//         }
         return new Integer((int) itineraries.get(0).getTotal_price());
      }
   }
   
   // Print the itineraries
   public void printItineraries() {
      if (this.itineraries != null) {
         System.out.println("Itinerary count: " + itineraries.size());
         // Display Itineraries returned
         it = itineraries.iterator();
         Itinerary tmp;
         while ( it.hasNext( ) ) {
            tmp = ( Itinerary ) it.next( );
            tmp.print( );
         }
      }
   }
}


public class QPX { 
   private String url = "https://www.googleapis.com/qpxExpress/v1/trips/search?key=AIzaSyDcCnj0LYJVMHnJB221Z_35ImwTER3tq6A"; // Ragnell
//   private String url = "https://www.googleapis.com/qpxExpress/v1/trips/search?key=AIzaSyC7-WhMITfNPw4DZLwPU2BS6IiFatdl2Ys";// Bradshaw
   private Time departure;
   private Time returnHome;
   private TripSet trip;
   private Set<String> destinationCodes;
   private String origin;
   private Integer passengers;
   private Integer solutionCount;
   private ArrayList<JSONObject> requests;
   private ArrayList<String> responses;
   private ArrayList<TripSet> tripList;
   private AirportFinder doesAirportExistObject;
  
   private Iterator it;

   /**
    * Given date, origin, and destination, creates JSONObject known as 
    * slice to QPX 
    */
   @SuppressWarnings("unchecked")
   private JSONObject createSlice( String date, String origin, String destination) {
      JSONObject obj = new JSONObject();
      obj.put("date", date);
      obj.put("origin", origin);
      obj.put("destination", destination);
      return obj;
   }
    
   /**
    * Given the array of destinations, create new requests for
    * each origin destination pair.
    * Pre-requests:
    *    Origin is defined with valid IATA code
    * Post Conditions:
    *    Internal requests array is filled with valid requests
    *    ready to be sent to QPX
   */ 
   @SuppressWarnings("unchecked")
   private void populateRequests () {
      String currentDestination;
      JSONObject subRequest;
      JSONObject request;
      JSONArray arr;
      
      requests = new ArrayList<JSONObject>();
      it = this.destinationCodes.iterator();
      
      // How many passengers are on this trip?
      JSONObject passengers = new JSONObject();
      passengers.put("adultCount", getPassengers());
      
//      System.out.println("Destinations: " + this.destinationCodes);

      while (it.hasNext()) {
         subRequest = new JSONObject();
         currentDestination = it.next().toString();
         subRequest.put("refundable", false);
         subRequest.put("solutions", this.solutionCount);
         arr = new JSONArray();
         // To flight
         arr.add( this.createSlice(getDeparture(), this.origin, currentDestination)); 
         // Return flight
         arr.add( this.createSlice(getReturn(), currentDestination, this.origin));
         
         subRequest.put("slice", arr);
         subRequest.put("passengers", passengers);
         
         // Display / Debug 
//         this.displayRequest(subRequest);
         
         // Build actual request object object
         request = new JSONObject();
         request.put("request", subRequest);
//         System.out.println(request.toString());
         requests.add(request);
      }
   }

   /**
    * Given string with JSON syntax, return a JSONObject representing the
    * results from the 'trips' object held internally
    * The returned object is used to define an itinerary
    * Pre-request: 
    *    trips must be a valid key
   */
   private JSONObject getTrips(String responseStr) throws ParseException {
      JSONParser responseFileParser = new JSONParser();
      JSONObject response = (JSONObject) responseFileParser.parse(responseStr);
      JSONObject trips = (JSONObject) response.get("trips");
      return trips;
   }
   
   /**
    * Given the requests array that is populated with QPX requests, submit the requests
    * to QPX and create itineraries from the responses
    * Pre-request: 
    *    requests array must be populated. ie, populateRequests must have been
    *    called
   */
   private void populateResponses() throws IOException, ParseException {
      this.responses = new ArrayList<String>();
      this.tripList = new ArrayList<TripSet>();
      
//      System.out.println("Making " + this.requests.size() + " QPX Express Requests");
      for (int i = 0; i < this.requests.size(); i++) {
         this.responses.add(this.QPXMakeQuery(requests.get(i)));
         // Need to populate triplist with itineraries
         this.tripList.add(new TripSet(getTrips(this.responses.get(i))));
      } 
   }

   /**
    * Given a JSONObject object crafted for QPX, make the QPX API call.
    * Pre-request:
    *    request must be a valid QPX query JSONObject 
    */
   private String QPXMakeQuery(JSONObject request) throws IOException, ParseException {
      // Set up connecton
      URL qpxUrl = new URL(url);
      HttpURLConnection qpxConnection = (HttpURLConnection) qpxUrl.openConnection();
      qpxConnection.setDoInput (true);
      qpxConnection.setDoOutput (true);
      qpxConnection.setRequestMethod("POST");
      qpxConnection.setRequestProperty("Content-Type", "application/json");
      
      // Give request
      OutputStreamWriter output = new OutputStreamWriter(qpxConnection.getOutputStream());
      output.write(request.toJSONString());
      output.flush();

      // Prep for response
      StringBuilder requestOutput = new StringBuilder();
      InputStream input;
      if(qpxConnection.getResponseCode() >= 400){
    	  input = (qpxConnection.getErrorStream());
    	  System.out.println(input.toString());
      }
      else{
    	  input = (qpxConnection.getInputStream());
      }
      
      // Read response
      int grab = 0;
      while((grab = input.read()) >= 0){
         requestOutput.append((char)grab);
      }

      return requestOutput.toString();
   }
     
   // ********************************** PUBLIC FUNCTIONS
   /**
    * Constructs new object.
    * Post Conditions:
    *    destinationCodes set is initialized
    *    solutionCount initialized to 5 (cheapest solution is first)
    *    passengers initialized to 1
    *    doesAirportExistObject initialized to new AirportFinder object
   */
   public QPX() {
      destinationCodes = new TreeSet<String>();
      solutionCount = new Integer( 5 );
      passengers = new Integer(1);
      doesAirportExistObject = new AirportFinder();
   }  

   /**
    * Inernal main for testing of QPX class and internal interactions with 
    * data members
   */
   public static void main(String[] args) throws IOException, ParseException {
      QPX flights = new QPX();
      flights.setDeparture("2016-01-25");
      flights.setReturn("2016-01-30");
      flights.setOrigin("DFW");
      flights.addDestinationCode("HDN");
//      flights.addDestinationCode("COS");
//      flights.addDestinationCode("DEN");
//      flights.addDestinationCode("DRO");
//      flights.addDestinationCode("EGE");
      flights.setPassengers(2);
      flights.getPricing(); // Makes actual QPX calls
   }
   
   /**
    * After , get pricing will return the cheapest 
    * price for the given number of passengers to each destination from origin.
    * If no flight exists cost is set to be average of existing
    * Pre-conditions:
    *    passenger count defined
    *    origin defined 
    *    departure date defined
    *    return date defined
    *    destinations have been set
    * Post Conditions:
    *    Requests array populated
    *    Responses array populated
   */
   public Map<String, Integer> getPricing() throws IOException, ParseException {
      this.populateRequests();
      this.populateResponses(); // Makes actual QPX Calls
      
      Map<String, Integer> pricing = new HashMap<String, Integer>();
      String currentDestination;
      
      it = this.destinationCodes.iterator();
      Double sumOfPrices = 0.0;
      Double priceNumber = 0.0;
      Double averagePrice = 0.0;
      int i = 0;
      while (it.hasNext()) {
         currentDestination = it.next().toString();
         Integer lowestPrice = tripList.get(i).getLowestPrice();
         if(lowestPrice > -1){
        	 sumOfPrices += lowestPrice;
        	 priceNumber++;
         }
         pricing.put(currentDestination, lowestPrice);
         i++;
      }
      // For the pricing that has not been defined, define it as the average
      //    of the other flights
      averagePrice = sumOfPrices / priceNumber;
      System.out.println(averagePrice + " is the average cost of airfare");
      for(Map.Entry<String, Integer> entry : pricing.entrySet()){
    	  if(entry.getValue() == -1){
    		  entry.setValue(averagePrice.intValue());
    	  }
      }
      
      return pricing;
   }

   // UTILITIES 
   /**
    * Internal functionality to save the request for later use
   */
   private void writeRequest(String QPX_Request) throws IOException {
      ObjectMapper mapper = new ObjectMapper();
      Object jsonString = mapper.readValue(QPX_Request, Object.class);
//      System.out.println(mapper.writerWithDefaultPrettyPrinter().writeValueAsString(jsonString));
      PrintWriter out = new PrintWriter(new FileWriter("request.json"));
      out.write(mapper.writerWithDefaultPrettyPrinter().writeValueAsString(jsonString));
      out.close();
   }

   /**
    * Internal functionality to display a JSONArray
    * which just happen to be origin-destination pairs
   */
   public void displayJSONArray(JSONArray arr) {
      System.out.println("Desired origin-destination pairs:");
      Iterator it = arr.iterator();
      while (it.hasNext()) {
         System.out.println("\t" + it.next().toString());
      }
   }
   /**
    * Internal functionality to display JSONObject
    * Function should only display requests. 
   */
   public void displayRequest(JSONObject obj) {
      System.out.println("Request:");
      System.out.println("\tPassenger Count: " + obj.get("passengers"));
      System.out.println("\tSolution Count: " + obj.get("solutions"));
      System.out.println("\tRefundable: " + obj.get("refundable"));
      JSONArray arr = (JSONArray) obj.get("slice");
      this.displayJSONArray(arr);
      System.out.println("");
   }

   /**
    * Internal functionality to save the response for later use
   */
   @SuppressWarnings("unused")
   private void saveResponse(String QPX_Results) throws IOException {
//    Save response to new file
      ObjectMapper mapper = new ObjectMapper();
      Object jsonString = mapper.readValue(QPX_Results, Object.class);
      PrintWriter out = new PrintWriter(new FileWriter("response.json"));
      out.write(mapper.writerWithDefaultPrettyPrinter().writeValueAsString(jsonString));
      out.close();
   }

   /**
    * NOTE: Kept for debugging
    * Makes QPX call based on saved request in file 'request.json' and returns 
    * response as string
    * Pre-requests: 
    *    request.json must contain valid JSONObject tailored to QPX  
   */
   // 
   public String QPXOutput() throws IOException, ParseException{
      String url = "https://www.googleapis.com/qpxExpress/v1/trips/search?key=AIzaSyDcCnj0LYJVMHnJB221Z_35ImwTER3tq6A";
      URL qpxUrl = new URL(url);
      HttpURLConnection qpxConnection = (HttpURLConnection) qpxUrl.openConnection();
      qpxConnection.setDoInput (true);
      qpxConnection.setDoOutput (true);
      qpxConnection.setRequestMethod("POST");
      qpxConnection.setRequestProperty("Content-Type", "application/json");
      
      JSONParser fileParser = new JSONParser();
      Object obj = fileParser.parse(new FileReader("request.json"));
      JSONObject request = (JSONObject) obj;
      
      OutputStreamWriter output = new OutputStreamWriter(qpxConnection.getOutputStream());
      output.write(request.toString());
      output.flush();
      
      StringBuilder requestOutput = new StringBuilder();
      InputStream input = (qpxConnection.getInputStream());
      int grab = 0;
      while((grab = input.read()) >= 0){
         requestOutput.append((char)grab);
      }
      
      return requestOutput.toString();
   }

   /**
    * Set return date
    * Pre-request: 
    *    date formatted yyyy-mm-dd
    */
   public void setReturn(String date) {
      returnHome = new Time(date + " 00:00");
   }
   /**
    * Set departure date
    * Pre-request: 
    *    date formatted yyyy-mm-dd
    */
   public void setDeparture(String date) {
      departure = new Time(date + " 00:00");
   }
   public String getReturn() {return returnHome.getDate();}
   public String getDeparture() {return departure.getDate();}
   
   /**
    * Add a destination to the destinations list
    * Must be valid IATA code to be successfully added
    * true returned if successful
    */ 
   public boolean addDestinationCode(String newCode) {
      if (doesAirportExistObject.doesThisAirportExist(newCode))
         return destinationCodes.add(newCode);
      else 
      {
//         System.out.println("ERROR: " + newCode + " does not exist."); 
         return false;
      }
   }

   /**
    * Set the origin of the flights
    * Must be valid IATA code to be successfully set
    * true returned if successful in setting origin
   */
   public boolean setOrigin(String origin) {
      if (doesAirportExistObject.doesThisAirportExist(origin)) {
         this.origin = origin;
         return true;
      }
      else 
         return false;
   }
   public String getOrigin() {return origin;}

   /**
    * Set the number of passengers
    * Pre-condition:
    *    Positive integer please
   */
   public void setPassengers(Integer count) {
      passengers = count;
   }
   public Integer getPassengers() {return passengers;}
   

}
