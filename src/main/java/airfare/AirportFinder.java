package airfare;

import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;



public class AirportFinder {
	private String urlLatLong = "https://airport.api.aero/airport/nearest/";
	private String urlName = "https://airport.api.aero/airport/match/";
	private String urlExists = "https://airport.api.aero/airport/";
	private Double latitude;
	private Double longitude;
	private String searchString;
	private Integer maxAirports = 1;
	private String userKey = "user_key=16834bd471c4d039f4748082f26d5db2";
	
	public static void main(String[] args) throws IOException, JSONException, ParseException{
		
		AirportFinder finder = new AirportFinder(31.54, -97.14);
		Airport closestAirport = finder.latLonAirportQuery();
		System.out.println(closestAirport.toString());
		if(   finder.doesThisAirportExist("DFW")) System.out.println("DFW Exists");;
      finder.doesThisAirportExist("ZZZ");
      finder.doesThisAirportExist("DEN");
      finder.doesThisAirportExist("DRO");
      finder.doesThisAirportExist("EGE");
      finder.doesThisAirportExist("COS");
	}
	
	public AirportFinder(Double lat, Double lon){
		latitude = lat;
		longitude = lon;
		this.latLongUrlConstructer();
	}
	
	public AirportFinder() {}
	
	public AirportFinder(String city){
		searchString = city;
		this.cityUrlConstructor();
	}
	
	public AirportFinder(Double lat, Double lon, Integer airports){
		latitude = lat;
		longitude = lon;
		maxAirports = airports;
		this.latLongUrlConstructer();
	}
	
	private void cityUrlConstructor() {
		while(searchString.contains(" ")){
			searchString.replace(" ", "%20");
		}
		urlName += searchString + "?" + userKey;
	}

	private void latLongUrlConstructer(){
		this.urlLatLong += latitude.toString() + "/" + longitude.toString() + "?";
		this.urlLatLong += "maxAirports=" + maxAirports.toString() + "&" + userKey;
	}
	
	public boolean doesThisAirportExist(String IATA) {
	   String constructedURL = urlExists + IATA + "?" + userKey;
	   
	   URL airportUrl;
      try {
         airportUrl = new URL(constructedURL);
         HttpURLConnection airportFinderConnection = (HttpURLConnection) airportUrl.openConnection();
         airportFinderConnection.setDoInput (true);
         airportFinderConnection.setDoOutput (true);
         airportFinderConnection.setRequestMethod("GET");
         airportFinderConnection.setRequestProperty("Content-Type", "application/json");
         
         StringBuilder requestOutput = new StringBuilder();
          InputStream input = (airportFinderConnection.getInputStream());
          int grab = 0;
          int count = 0;
          while((grab = input.read()) >= 0){
            if(count > 8 && (char)grab != ')'){
               requestOutput.append((char)grab);
            }
            else{
               count++;
            }
          }
          String requestOutputString = requestOutput.toString();
          JSONObject response = new JSONObject(requestOutputString);
          if ((boolean)response.get("success"))
             return true;
         return false;
      } catch (MalformedURLException e) {
         // TODO Auto-generated catch block
         e.printStackTrace();
      } catch (JSONException e) {
         // TODO Auto-generated catch block
         e.printStackTrace();
      } catch (IOException e) {
         // TODO Auto-generated catch block
         e.printStackTrace();
      }
	   return false;
	}
	public Airport latLonAirportQuery() throws IOException, JSONException{
		Airport closestAirport = new Airport();
		URL airportUrl = new URL(this.urlLatLong);
		HttpURLConnection airportFinderConnection = (HttpURLConnection) airportUrl.openConnection();
		airportFinderConnection.setDoInput (true);
		airportFinderConnection.setDoOutput (true);
		airportFinderConnection.setRequestMethod("GET");
		airportFinderConnection.setRequestProperty("Content-Type", "application/json");
		
		StringBuilder requestOutput = new StringBuilder();
	    InputStream input = (airportFinderConnection.getInputStream());
	    int grab = 0;
	    int count = 0;
	    while((grab = input.read()) >= 0){
	    	if(count > 8 && (char)grab != ')'){
	    		requestOutput.append((char)grab);
	    	}
	    	else{
	    		count++;
	    	}
	    }
	    String requestOutputString = requestOutput.toString();
	    JSONObject data = new JSONObject(requestOutputString);
	    
	    String cityName;
	    String letterCode;
	    JSONObject givenAirport;
	    JSONArray airports = data.getJSONArray("airports");
	    givenAirport = (JSONObject) airports.get(0);
	    cityName = givenAirport.get("city").toString();
	    letterCode = givenAirport.get("code").toString();
	    closestAirport.setCity(cityName);
	    closestAirport.setCode(letterCode);
	    
		return closestAirport;
	}
	
	
	
	public Airport nameAirportQuery(){
		Airport closestAirport = null;
		
		return closestAirport;
	}
	
	
}
