package airfare;

import org.json.simple.JSONObject;

class City {
private String code;
private String name;
public City() {}
public City(JSONObject obj) {
 code = obj.get("code").toString();
 name = obj.get("name").toString();
}
public String getCode() {return code;}
public String getName() {return name;}
public void setName(String name) {
 this.name = name;
}
public String toString() {
 return getCode() + " => " + getName();
}

}
public class Airport {
	
   private String code;
   private String name;
   private City city;
   public Airport() {
   }
   public Airport(JSONObject obj) {
      code = obj.get("code").toString();
      name = obj.get("name").toString();
   }
   public String getCode() {return code;}
   public String getName() {return name;}
   public String toString() {
      return getCode() + " => " + getName();
   }
   
   public void setCity(String c){
	   City temp = new City();
	   temp.setName(c);
	   this.name = temp.getName();
   }
   
   public void setCode(String c){
	   this.code = c;
   }
		
}
